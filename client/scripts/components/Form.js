import html from '../util/html'

const Form = (state, actions) => html`
  <form id="form" onsubmit="${e => actions.send(e)}">
    <input type="text" oninput="${e => actions.set({ key: 'text', value: e.target.value })}" value="${state.text}" required />
    <button type="submit">Send</button>
  </form>
`

export default Form