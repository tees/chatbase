import html from '../util/html'

const Login = (state, actions) => html`
  <form id="login" onsubmit="${e => actions.login(e)}">
    <h1>Sign in</h1>
    <p>
      <label>Username
        <input
          name="username"
          type="username"
          value="${state.user.username}"
          required
          oninput="${e => actions.set({
            type: 'user',
            key: 'username',
            value: e.target.value
          })}"
        />
      </label>
    </p>
    <button type="submit">Start chatting</button>
  </form>
`

export default Login