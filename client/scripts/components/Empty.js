import html from '../util/html'

const Empty = html`
  <h4>No messages yet...</h4>
`

export default Empty