import html from '../util/html'
import Messages from './Messages'
import Form from './Form'

const Chat = state => html`
  <div id="chat">
    ${Messages}
    ${Form}
  </div>
`

export default Chat