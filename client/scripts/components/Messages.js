import { h } from 'hyperapp'
import html from '../util/html'
import Empty from './Empty'
import Message from './Message'

let previousMessageUser

const Messages = state => html`
  <div id="messages">
    ${
      !state.messages || state.messages.length === 0
      ? Empty
      : state.messages.map(message => {
          const showAvatar = message.email !== previousMessageUser
          previousMessageUser = message.email
          return Message(message, showAvatar)
        })
    }
  </div>
`

export default Messages