import { h } from 'hyperapp'
import html from '../util/html'
import dayjs from 'dayjs'
import gravatar from 'gravatar'

let previousMessageUser

const Message = (message, showAvatar) => {
  const timestamp = dayjs(message.timestamp).format('MM/DD/YYYY HH:mm:ssa')
  const avatarUrl = gravatar.url(message.email)

  return html`
    <div class="message" title="${timestamp}">
      ${
        showAvatar
          ? html`<img class="avatar" src="${avatarUrl}" alt="${message.username}" />`
          : html`<span class="spacer"></span>`
      }
      <span class="text">${message.text}</span>
    </div>
  `
}

export default Message