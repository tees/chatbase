import { h, app } from 'hyperapp'
import html from './util/html'
import socket from './util/socket'
import state from './state'
import actions from './actions'
import Chat from './components/Chat'
import Login from './components/Login'
import '../styles/main.scss'

const view = (state, actions) => {
  if (state.user.username && state.user.isAuthenticated) {
    return Chat
  } else {
    return Login
  }
}

const emit = app(state, actions, view, document.body)

socket.on('messages', messages => {
  emit.set({ key: 'messages', value: messages })
})
