import socket from './util/socket'

const actions = {
  set: ({ type, key, value })  => state => {
    if (type) {
      return {
        [type]: Object.assign({}, state[type], {
          [key]: value
        })
      }
    } else {
      return {
        [key]: value
      }
    }
  },
  login: event => state => {
    event.preventDefault()

    // const credentials = {
    //   email: state.user,
    //   password: state.password
    // }

    // // const user = await auth(credentials)
    // const user = {
    //   username: 'tylersnyder',
    //   email: 'tylersnyder@gmail.com'
    // }

    return {
      user: Object.assign({}, state.user, {
        isAuthenticated: true
      })
    }
  },
  send: event => state => {
    event.preventDefault()

    const message = {
      email: state.user.email,
      timestamp: Date.now(),
      text: state.text
    }

    socket.send(message)
    return { text: '' }
  }
}

export default actions