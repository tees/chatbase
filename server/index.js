const app = require('express')()
const server = require('http').Server(app)
const io = require('socket.io')(server)
const db = require('./util/db')
const Bundler = require('parcel-bundler')

const bundler = new Bundler('client/index.html', {
  outDir: './client/dist',
  cacheDir: './client/.cache'
})

app.use(bundler.middleware())

server.listen(3000)

io.on('connection', async (socket) => {
  const messages = await db.messages.get()
  socket.emit('messages', messages)

  socket.on('message', async (message) => {
    await db.setMessage(message)
    const messages = await db.messages.get()
    socket.emit('messages', messages)
  })
})
