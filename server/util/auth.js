const { Basic } = require('permit')
const db = require('./db')

const permit = new Basic({
  query: ['email', 'password']
})

module.exports = async (req, res, next) => {
  try {
    console.log(permit.check(req))
    const [ email, password ] = permit.check(req)

    if (!email || !password) {
      permit.fail(res)
      throw new Error('Unauthorized')
    }

    const user = await db.users.findByEmail(email)

    if (!user || user.password !== password) {
      permit.fail(res)
      throw new Error('Unauthorized')
    }

    req.user = user
    next()
  } catch (error) {
    console.error(error)
    next()
  }
}