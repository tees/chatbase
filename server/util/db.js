const admin = require('firebase-admin')
const serviceAccount = require('./firebase.json')

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://cheddar-6848e.firebaseio.com'
})

const db = admin.firestore()

module.exports = {
  messages: {
    get: () => db.collection('messages')
      .orderBy('timestamp', 'asc')
      .get()
      .then(snapshot => {
        let messages = []

        snapshot.forEach(doc => {
          messages = [...messages, doc.data()]
        })
        
        return messages
      }),
    set: message => db.collection('messages').add(message)
  },
  users: {
    findByEmail: email => db.collection('users')
      .where('email', '==', email)
      .get()
      .then(snapshot => {
        console.log(snapshot)
        return snapshot
      })
  }
}